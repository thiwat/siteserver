const ReturnMessage = (result, message) => {
    return { result, message }
}

exports.SUCCESS = ReturnMessage(1, 'Success')

exports.NO_PERMISSION = ReturnMessage(0, 'no permission')

exports.WRONG_KEY = ReturnMessage(0, 'Wrong secret key')

exports.CODE_INVALID = ReturnMessage(0, 'Invalid code')

exports.CODE_EXPIRE = ReturnMessage(0, 'Code expired')

exports.ALREADY_USED = ReturnMessage(0, 'This code already used')

exports.NO_CARD = ReturnMessage(0, 'This card is not exist')

exports.DUP_WIEGAND = ReturnMessage(0, 'This wiegand number already exist')

exports.DUP_CARD = ReturnMessage(0, 'This card already used')

exports.NO_IMAGE = ReturnMessage(0, "Image not found")

exports.OUT_OF_SERVICE = ReturnMessage(0, "Out of service")