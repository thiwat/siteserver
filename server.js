const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const { logger, requestLogger } = require('./log')
const { checkUpdate } = require('./utils/request')
const { checkSecret } = require('./utils/auth')

require('dotenv').config()
process.env.NODE_DIR = __dirname

const app = express()
const port = process.env.PORT

require('./api/models/userModel')
require('./api/models/visitorModel')
require('./api/models/tempModel')

function gracefulShutdown() {
    logger('INFO', 'Got terminate signal, Server is shutting down...')
    server.close(() => {
        logger('INFO', 'Shutdown server success')
        process.exit()
    })

    const timeout = 10 * 1000 // 10 second
    setTimeout(() => {
        logger('ERR', 'Shutdown server unsuccess, Force shutdown server')
        process.exit(1)
    }, timeout)
}

process.on('SIGTERM', gracefulShutdown)
process.on('SIGINT', gracefulShutdown)

setInterval(checkUpdate, 10000)

mongoose.connect(process.env.MONGODB_URL, { useNewUrlParser: true })
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection err : '))

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(express.static(__dirname + '/static'))

app.use(requestLogger)
app.use(checkSecret)

const userRoute = require('./api/routes/userRoute')
const visitorRoute = require('./api/routes/visitorRoute')
const receiverRoute = require('./api/routes/receiverRoute')
userRoute(app)
visitorRoute(app)
receiverRoute(app)

const server = app.listen(port)

const { initial, sendReceiver } = require('./utils/socket')
initial(server)
setInterval(sendReceiver, 15000)

logger('INFO', `Server is ready for listen and serve with port ${port}`)


module.exports = app