var socketio = io.connect("http://" + document.domain + ":" + location.port)
var GraphList = {}
var userList = []

socketio.on("connect", function(){
    initialEditUser()
})

socketio.on("test", function(data){
    initialGraph(data)
})

socketio.on("testentry", function(data) {
    if (data['entryList'] !== null) {
        tempUpdateEntryList(data)
    }
})

socketio.on('request', function(data) {
    addApproveUser(data)
})

socketio.on('users', function(data) {
    userList = data
    addUser(data)
})

socketio.on("showEditUser", function(data){
    showEditUser(data.id, data.name, data.phoneNumber, data.email, data.address, data.wiegand, data.rfid)
})


function initialGraph(graphList) {
    for (let i=0, max=graphList.length ; i < max ; i++ ) {
        let graph = graphList[i]
        if (!(graph.name in GraphList)) {
            GraphList[graph.name] = {"category": graph.category, "count": {}}
        }
        GraphList[graph.name]["count"][graph.entry] = graph.count
    }
    updateGraph(GraphList)
}

socketio.on("update", function(data){
    entryMessage = data['entryList']
    graphMessage = data['receiverList']

    if (entryMessage !== null) {
        tempUpdateEntryList(entryMessage)
    }
    initialGraph(graphMessage)
})


socketio.on("approveUser", function(data){
    addApproveUser(data['requestList'])
})

socketio.on("receiver", function(data){
    newUpdateReceiver(data)
})

//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
function firstUpperCase(string){
    return string.charAt(0).toUpperCase() + string.slice(1)
}

var page_list = null
function switchPage(id){
    let icon_page
    if (page_list === null) page_list = document.getElementsByClassName("page")
    for(var i=0; i<page_list.length; i++){
        if(id+"-page" == page_list[i].id){
            page_list[i].style.display = "block"
            icon_page = document.getElementById(id)
            icon_page.classList.add("selected")
            icon_page.classList.remove("unselected")
        }
        else{
            page_list[i].style.display = "none"
            icon_page = document.getElementById(page_list[i].id.slice(0, -5))
            icon_page.classList.add("unselected")
            icon_page.classList.remove("selected")
        }
    }
}


//////////////////////////////////////////////////////
////////////////////////////////////////////////////

var recent_table = null

function updateEntryList(entry_list){
    if (recent_table === null) recent_table = document.getElementById("recent-activity")
    var name, icon, gate, entry, time, data
    var row, name_item, icon_item, gate_item, entry_item, time_item
    var buildElement = function(tag, class_name, src, text){
        var item = document.createElement(tag)
        item.className = class_name
        if (src !== null) item.src = src
        if (text !== null) item.innerHTML = text
        return item
    }
    for (var i=0; i<entry_list.length; i++){
        
        data = entry_list[i]
        name = data['name']
        image_path = data['imagePath']
        gate = data['category']
        entry = data['entry']
        time = data['time']

        row = buildElement("div", "block-row recent-block", null, null)
        row.appendChild(buildElement("img", "block-row-icon meterial-icons", image_path, null))
        row.appendChild(buildElement("p", "block-row-name", null, name))
        row.appendChild(buildElement("img", "block-row-gate", "/img/" + gate + ".png", null))
        row.appendChild(buildElement("img", "block-row-status", "/img/"+entry+".png", null))
        row.appendChild(buildElement("p", "block-row-time", null, time))

        recent_table.insertBefore(row, recent_table.firstChild)

    }
}
var recentTable = null
function tempUpdateEntryList(entryList) {
    if (recentTable === null) recentTable = document.getElementById("recent-activity")
    var buildElement = function(tag, className, src, text) {
        var item = document.createElement(tag)
        item.className = className
        if (src !== null) item.src = src
        if (text !== null) item.innerHTML = text
        return item
    }

    var data, name, imagePath, gate, category, entry, time
    for (var i=0, max=entryList.length ; i<max ; i++ ) {
        data = entryList[i]
        name = data['name']
        imagePath = data['imagePath']
        gate = data['gateName']
        category = data['gateCategory']
        entry = data['entry']
        time = data['time']

        row = buildElement("div", "block-row recent-block", null, null)
        row.appendChild(buildElement("img", "block-row-icon meterial-icons", imagePath, null))
        row.appendChild(buildElement("p", "block-row-name", null, name))
        row.appendChild(buildElement("img", "block-row-gate", "/img/" + category + ".png", null))
        row.appendChild(buildElement("img", "block-row-status", "/img/" + entry + ".png", null))
        row.appendChild(buildElement("p", "block-row-time", null, time))

        recentTable.insertBefore(row, recentTable.firstChild)
    }
}

var entry_graph = null
var block_graph = null
var block_width = 0
var gate_num = 0
var svg_ns = "http://www.w3.org/2000/svg"
var bar_max_height = 70
var bar_bottom = 80
var bar_width = 0
var graph_max_value = 10
var graph_step = [2, 2.5, 2]
var graph_index = 10

function initGraph(gate_list){
    entry_graph = document.getElementById("report-graph")
    block_graph = document.getElementById("report-graph2")
    gate_num = Object.keys(gate_list).length
    block_width = 100 / gate_num
    bar_width = block_width / 3
    var item
    var buildElement = function(type, attr_list){
        item = document.createElementNS(svg_ns, type)
        for(var key in attr_list){
            item.setAttributeNS(null, key, attr_list[key])
        }
        return item
    }

    var i = 0
    var icon, bar, value, entry_list

    for(var gate in gate_list){
        let x, y, h, entry
        let block_center = block_width * (2 * i + 1) / 2
        icon = buildElement("image", {"href":"/img/"+gate_list[gate]["category"]+".png", "width":"20%", "height":"20%", "x":(block_center-10)+"%", "y":"82%"})
        block_graph.appendChild(icon)
        entry_list = Object.keys(gate_list[gate]["count"])
        if (entry_list.length == 1){
            x = block_center - bar_width / 2
            entry = entry_list[0]
            bar = buildElement("rect", {"x":x+"%", "y":(-1*bar_bottom)+"%", "width":bar_width+"%", "height":"0%", "class":"graph-bar "+entry, "gate":gate, "entry":entry})
            entry_graph.appendChild(bar)
        }
        else{
            for(var j in entry_list){
                entry = entry_list[j]
                if (entry == "in") x = block_center - bar_width
                else x = block_center
                bar = buildElement("rect", {"x":x+"%", "y":(-1*bar_bottom)+"%", "width":bar_width+"%", "height":"0%", "class":"graph-bar "+entry, "gate":gate, "entry":entry})
                entry_graph.appendChild(bar)
            }
        }
        i += 1
    }
}

function updateGraph(gate_list){
    if (entry_graph === null) initGraph(gate_list)
    var findGraphParameter = function(gate_list){
        var max_value = 0
        for(var gate in gate_list){
            for(var entry in gate_list[gate]['count']){
                if (gate_list[gate]['count'][entry] > max_value) max_value = gate_list[gate]['count'][entry]
            }
        }

        while (max_value > graph_max_value){
            graph_max_value = graph_max_value * graph_step[graph_index%3]
            graph_index += 1
        }
    }

    findGraphParameter(gate_list)
    var bar_list = entry_graph.getElementsByTagNameNS(svg_ns, "rect")
    var gate, entry, height, old_height, step_size
    var drawGraph = function(bar, height, step_size, index){
        if (height < 0) height = 0
        bar.setAttributeNS(null, "height", height+"%")
        if(index < 20) setTimeout(function(){ drawGraph(bar, height+step_size, step_size, index+1)}, 20)
    }
    for(var i=0; i<bar_list.length; i++){
        gate = bar_list[i].getAttributeNS(null, "gate")
        entry = bar_list[i].getAttributeNS(null, "entry")
        old_height = parseFloat(bar_list[i].getAttributeNS(null, "height").slice(0, -1))
        height = gate_list[gate]['count'][entry] * bar_max_height / graph_max_value
        step_size = (height - old_height) / 20
        drawGraph(bar_list[i], old_height+step_size, step_size, 1)
    }
}



var approve_table = null

function addApproveUser(user_list){
    if (approve_table === null) approve_table = document.getElementById("manage-body")
    var row, idx, name_col, email_col, checkbox_col, button_col
    var button_format, approve_button, reject_button, disable
    var buildElement = (tagName, text, classList) => {
        let item = document.createElement(tagName)
        if (text !== null) {
            item.innerHTML = text
        }
        item.setAttribute("class", classList)
        return item
    }
    var buildButton = (phone, className, mode, text, status, mail) => {
        let item = document.createElement("button")
        item.innerHTML = text
        if (status != "") item.disabled = true
        item.setAttribute("class", className)
        item.setAttribute("onclick", `approveUser(this, '${phone}', '${mode}', '${mail}')`)
        return item
    }
    var addFunction = function(phoneNumber, name, email, status){
        row = buildElement("div", "", "manage-row")
        rowIdx = approve_table.getElementsByClassName("manage-row").length 
        row.appendChild(buildElement("div", name, "manage-name-col manage-col"))
        row.appendChild(buildElement("div", email, "manage-email-col manage-col"))
        approveButton = buildButton(phoneNumber, "manage-button manage-button-approve", "add", "Approve", status, email)
        rejectButton = buildButton(phoneNumber, "manage-button manage-button-reject red", "reject", "Reject", status, email)
        buttonCol = buildElement("div", null, "manage-button-col manage-col")
        buttonCol.appendChild(rejectButton)
        buttonCol.appendChild(approveButton)
        row.appendChild(buttonCol)
        approve_table.appendChild(row)
    }
    clearTable()
    if (user_list === null) {
        return
    }
    var userData
    for(var i=0;i<user_list.length;i++){
        userData = user_list[i]
        addFunction(userData.phoneNumber, userData.name, userData.email, userData.status)
    }
}

function clearTable(){
    approve_table.innerHTML = ""
}


async function approveUser(r, phoneNumber, status, email){
    var parent = r.parentNode
    var buttons = parent.getElementsByTagName("button")
    for(var i=0; i<buttons.length; i++){
        buttons[i].disabled = true
    }
    const result = await fetch('/approve', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            phoneNumber,
            status,
            email,
            secretKey: '123123'
        })
    })
    if(result.status !== 200) {
        const resultJson = await result.json()
        alert(resultJson.message)
    }
}

var receiver_table = null

function updateReceiver(receiver_list){
    if (receiver_table === null) receiver_table = document.getElementById("receiver-list")

    var buildElement = function(class_name, text, attr_list){
        let item = document.createElement(class_name)
        for(let key in attr_list){
            item.setAttribute(key, attr_list[key])
        }
        if (text !== null) item.innerHTML = text
        return item
    }

    var receiver_row = receiver_table.getElementsByTagName("div")
    for( var i=0, max=receiver_row.length; i<max; i++){
        receiver_table.removeChild(receiver_row[0])
    }

    for( var i=0; i<receiver_list.length; i++){
        let receiver = receiver_list[i]
        let gate = receiver['gate'], entry = receiver['entry'], status = receiver['status']

        var row = buildElement("div", null, {"class":"receiver-row"})
        row.appendChild(buildElement("img", null, {"src":"static/img/"+gate+".png", "class":"block-row-icon"}))
        row.appendChild(buildElement("img", null, {"src":"static/img/"+entry+".png", "class":"block-row-entry"}))
        row.appendChild(buildElement("p", firstUpperCase(gate) + " [" + firstUpperCase(entry) +"]", {"class":"block-row-name"}))
        row.appendChild(buildElement("p", status, {"class":"block-row-status "+status}))

        receiver_table.appendChild(row)
    }
}

var receiverTable = null

function newUpdateReceiver(receiverList) {
   if (receiverTable === null) receiverTable = document.getElementById("receiver-list")

   var buildElement = function(className, text, attrList) {
       let item = document.createElement(className)
       for (let key in attrList) {
           item.setAttribute(key, attrList[key])
       }
       if (text !== null) item.innerHTML = text
       return item
   }
   var receiverRow = receiverTable.getElementsByTagName("div")
   for (var i=0, max=receiverRow.length; i<max ; i++) {
        receiverTable.removeChild(receiverRow[0])
   }

   for (var i=0; i<receiverList.length ; i++ ) {
       let receiver = receiverList[i]
       var row = buildElement("div", null, {"class":"receiver-row"})
       row.appendChild(buildElement("img", null, {"src":"/img/" + receiver.category + ".png", "class":"block-row-icon"}))
       row.appendChild(buildElement("img", null, {"src":"/img/"+receiver.entry+".png", "class":"block-row-entry"}))
       row.appendChild(buildElement("p", firstUpperCase(receiver.name), {"class":"block-row-name"}))
       row.appendChild(buildElement("p", receiver.status, {"class":"block-row-status " + receiver.status}))
       receiverTable.appendChild(row)
   }
}

var showName = null
var editTable = null
var showPhone, showEmail, showAddress, showRFID
var showWiegand, editButton, showImage, deleteButton, editInput
var textChange = false

function editUser(userId){
    const { id, name, phoneNumber, email, address, wiegand, card} = userList.find(item => item.id === userId)
    showEditUser(id, name, phoneNumber, email, address, wiegand, card)
}

function initialEditUser() {
    editInput = document.getElementById("edit-input")
}

function addUser(userList) {
    if (editTable === null) {
        editTable = document.getElementById("edit-table")
    }

    var buildElement = (className, text) => {
        let item = document.createElement("div")
        item.setAttribute("class", className)
        if (text !== null) {
            item.innerHTML = text
        }
        return item
    }

    var buildButton = (className, userId) => {
        let item = document.createElement("button")
        item.setAttribute("class", className)
        item.setAttribute("onclick", `editUser(${userId})`)
        item.innerHTML = "Edit"
        return item
    }

    clearEditTable()

    for (let i = 0, max = userList.length; i<max ; i++) {
        let user = userList[i]
        row = buildElement("edit-table-row", null)
        row.appendChild(buildElement("edit-table-name-col edit-table-col", user['name']))
        row.appendChild(buildElement("edit-table-card-col edit-table-col", user['card'] === '' ? '-' : user['card']))
        button = buildButton("edit-table-button", user['id'])
        buttonCol = buildElement("edit-table-button-col edit-table-col", null)
        buttonCol.append(button)
        row.appendChild(buttonCol)
        editTable.appendChild(row)
    }
}

function clearEditTable() {
    editTable.innerHTML = ""
}

function showEditUser(id, name, phone, email, address, wiegand, rfid) {
    if (showName === null) {
        showName = document.getElementById("show-name")
        showPhone = document.getElementById("show-phone")
        showEmail = document.getElementById("show-email")
        showAddress = document.getElementById("show-address")
        showWiegand = document.getElementById("show-wiegand")
        showRFID = document.getElementById("show-rfid")
        showImage = document.getElementById("show-image")
        editButton = document.getElementById("edit-button")
        deleteButton = document.getElementById("delete-button")
    }

    showName.innerHTML = name
    showPhone.innerHTML = phone
    showEmail.innerHTML = email
    showAddress.innerHTML = address
    showWiegand.value = wiegand
    showRFID.value = rfid
    showImage.src = "/users/" + phone + ".png"
    editButton.disabled = false
    deleteButton.disabled = false
    deleteButton.setAttribute("userId", id)
}

function textChangeHandle() {
    editButton.innerHTML = "Save"
}

async function startEditUser() {
    if (editButton.innerHTML == "Save") {
        if(showRFID === '' && showWiegand === '') {
            alert("please enter least one field")
            return
        }
        let userId = parseInt(deleteButton.getAttribute("userId"))
        const result = await fetch('/edituser', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: userId,
                card: showRFID.value,
                wiegand: showWiegand.value,
                secretKey: '123123'
            })
        })
        if(result.status === 422) {
            const { message } = await result.json()
            alert(message)
            return 
        }
        clearShowUser()
        alert("Edit user success")
    }
    else if(editButton.innerHTML == "Cancel") {
        clearShowUser()
    }
    else {
        editButton.innerHTML = "Cancel"
        showWiegand.disabled = false
        showRFID.disabled = false
    }
    showWiegand.classList.toggle("block")
    showRFID.classList.toggle("block")
}

function clearShowUser() {
    editButton.innerHTML = "Edit"
    showWiegand.disabled = true
    showRFID.disabled = true
    showName.innerHTML = ""
    showPhone.innerHTML = ""
    showEmail.innerHTML = ""
    showAddress.innerHTML = ""
    showWiegand.value = ""
    showRFID.value = ""
    showImage.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
    editButton.disabled = true
    deleteButton.disabled = true
}

async function deleteUser() {
    userId = parseInt(deleteButton.getAttribute("userId"))
    const result = await fetch('/delete', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            userId,
            secretKey: '123123'
        })
    })
    if(result.status !== 200) {
        const { message } = await result.json()
        alert(message)
        return
    }
    clearShowUser()
    alert("Delete User Success")
}

function searchFunction() {
    searchText = editInput.value.toUpperCase()
    userList = editTable.getElementsByClassName("edit-table-row")
    for(var i=0, max=userList.length; i < max ; i++ ) {
        row = userList[i]
        name = row.getElementsByClassName("edit-table-name-col")[0].innerHTML.toUpperCase()
        if (name.indexOf(searchText) == -1) {
            row.style.display = 'none'
        }
        else {
            row.style.display = ''
        }
    }
}