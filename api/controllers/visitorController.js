'use strict'
const mongoose = require('mongoose')
const VisitorModel = mongoose.model('visitors')
const UserModel = mongoose.model('users')
const { logger } = require('../../log')
const { SUCCESS, CODE_INVALID, CODE_EXPIRE, ALREADY_USED } = require('../../errors')

const code = async (req, res) => {
    const { id, entry, code } = req.body
    const visitor = await VisitorModel.findOne({ code })
    if(!visitor) {
        return res.status(404).json( CODE_INVALID )
    }

    const timeNow = Math.floor( new Date() / 1000 )
    if(visitor.expireDate < timeNow) {
        return res.status(400).json( CODE_EXPIRE )
    }

    const { used, name, userId, userName } = visitor
    if(used[id.toString()]) {
        return res.status(400).json( ALREADY_USED )
    }
    used[id.toString()] = true

    const { wiegand } = await UserModel.findOne({ id: userId })

    await VisitorModel.updateOne({ code }, {$set: { used }})
    return res.json({ ...SUCCESS, name, wiegand })
}

module.exports = { code }