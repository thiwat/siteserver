'use strict'
const mongoose = require('mongoose')
const fetch = require('node-fetch')
const UserModel = mongoose.model('users')
const TempModel = mongoose.model('temps')
const { logger } = require('../../log')
const { SUCCESS, NO_PERMISSION, NO_CARD, DUP_CARD, DUP_WIEGAND, OUT_OF_SERVICE } = require('../../errors')
const { userEntry } = require('../../utils/socket')
const { tick, tickTemp } = require('../../utils/socket')
const { saveUserImage } = require('../../utils/file')

const entry = async (req, res) => {
    const { id, gateId } = req.body

    const user = await UserModel.findOne({ id })
    if(!user) {
        logger('INFO', `user id ${id} entry without permission`)
        return res.status(404).json( NO_PERMISSION )
    }
    const { phoneNumber, name, wiegand } = user
    logger('INFO', `user id ${id} entry success`)
    userEntry(user.id, name, phoneNumber, gateId)
    return res.json({ ...SUCCESS, phone: phoneNumber, name, wiegand })
}

const card = async (req, res) => {
    const { id, card } = req.body
    const user = await UserModel.findOne({ card })
    if(!user) {
        return res.status(404).json( NO_CARD )
    }

    const { name, phoneNumber, wiegand } = user
    userEntry(user.id, name, phoneNumber, id)
    return res.json({ ...SUCCESS, name, phone: phoneNumber, wiegand })
}

const edit = async (req, res) => {
    const { id, card, wiegand } = req.body
    const query = []
    if(card !== '') { query.push({card})}
    if(wiegand !== '') { query.push({wiegand})}

    const check = await UserModel.findOne({ $and: [{ id: {$ne: id}}, { $or: query }]})
    if(check) {
        if(check.wiegand === wiegand) {
            return res.status(422).json( DUP_WIEGAND )
        }
        else if(check.card === card) {
            return res.status(422).json( DUP_CARD )
        }
    }
    await UserModel.updateOne({ id }, {$set: {card, wiegand }})
    tick()
    tickTemp()
    return res.json( SUCCESS )
}

const approve = async (req, res) => {
    const { phoneNumber, status, email } = req.body
    const result = await fetch(`${process.env.SERVER_URL}/approveuser`, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            siteCode: process.env.SITE_CODE,
            phoneNumber,
            email,
            status,
            secretKey: process.env.SECRET_KEY
        })
    })
    if(result.statue !== 200) {
        return res.status(400).json({ message: 'Maximum User Limit'})
    }
    if(status !== 'reject') {
        const user = await result.json()
        user['wiegand'] = ''
        user['card'] = ''
        saveUserImage(user['phoneNumber'], user['image'])
        tickTemp()
        await new UserModel(user).save()
    }
    await TempModel.deleteOne({ phoneNumber })
    tick()
    return res.json( SUCCESS )
}

const del = async (req, res) => {
    const { userId } = req.body
    const result = await fetch(`${process.env.SERVER_URL}/deleteuser`, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: userId,
            siteId: parseInt(process.env.SITE_ID),
            secretKey: process.env.SECRET_KEY
        })
    })
    if(result.status !== 200) {
        return res.status(500).json( OUT_OF_SERVICE )
    }
    await UserModel.deleteOne({ id: userId })
    tick()
    tickTemp()
    return res.json( SUCCESS )
}

module.exports = { entry, card, edit, approve, del }