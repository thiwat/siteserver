'use strict'
const { logger } = require('../../log')
const { SUCCESS, NO_IMAGE } = require('../../errors')
const { readUserImage, readTempFile } = require('../../utils/file')
const { updateReceiver } = require('../../utils/socket')

const receiverList = [{id: 1}]

const image = async (req, res) => { 
    const { phoneNumber } = req.body
    try {
        const image = await readUserImage(phoneNumber)
        return res.json({ ...SUCCESS, image })
    } catch (err) {
        return res.status(404).json( NO_IMAGE )
    }
}

const inform = async (req, res) => {
    const { id, lastUpdate } = req.body
    receiverList.find(item => item.id === id).lastUpdate = Math.floor(new Date() / 1000)
    const data = await readTempFile()
    const update = data.timeStamp !== lastUpdate ? data : {timeStamp: 0, userList: []}
    updateReceiver(id)
    return res.json({ ...SUCCESS, update })
}

module.exports = { image, inform }