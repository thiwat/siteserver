const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema ({
    id: Number,
    phoneNumber: String,
    name: String,
    email: String,
    address: String,
    wiegand: String,
    card: String
})

module.exports = mongoose.model('users', UserSchema)