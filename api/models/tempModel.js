const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TempSchema = new Schema ({
    name: String,
    phoneNumber: String,
    email: String,
    status: String
})

module.exports = mongoose.model('temps', TempSchema)