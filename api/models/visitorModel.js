const mongoose = require('mongoose')
const Schema = mongoose.Schema

const VisitorSchema = new Schema ({
    name: String,
    plate: String,
    userId: Number,
    userName: String,
    code: String,
    used: Object,
    expireDate: Number
})

module.exports = mongoose.model('visitors', VisitorSchema)