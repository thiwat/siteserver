'use strict'
const visitorController = require('../controllers/visitorController')

module.exports = function(app) {

    app.route('/codecheck')
        .post(visitorController.code)

}