'use strict'
const userController = require('../controllers/userController')

module.exports = function(app) {

    app.route('/entry')
        .post(userController.entry)

    app.route('/cardcheck')
        .post(userController.card)

    app.route('/edituser')
        .post(userController.edit)

    app.route('/approve')
        .post(userController.approve)

    app.route('/delete')
        .post(userController.del)
}