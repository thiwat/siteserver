'use strict'
const receiverController = require('../controllers/receiverController')

module.exports = function(app) {
    app.route('/image')
        .post(receiverController.image)

    app.route('/inform')
        .post(receiverController.inform)
}
