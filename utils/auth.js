'use strict'

const { logger } = require('../log')
const { WRONG_KEY } = require('../errors')

const checkSecret = (req, res, next) => {
    const { secretKey } = req.body
    const { remoteAddress } = req.connection
    if (secretKey !== process.env.SECRET_KEY) {
        logger('AUTH', `${remoteAddress} access with wrong secret key`)
        return res.status(401).json( WRONG_KEY )
    }
    next()
}

module.exports = { checkSecret }