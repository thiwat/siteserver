'use string'
const { logger } = require('../log')
const fetch = require('node-fetch')
const { saveUserImage } = require('../utils/file')
const VisitorModel = require('../api/models/visitorModel')
const TempModel = require('../api/models/tempModel')
const _ = require('lodash')
const gateList = require('../config/gateList.json')
const { tick } = require('../utils/socket')

const checkUpdate = async () => {
    try {
        const result = await fetch(`${process.env.SERVER_URL}/siteupdate`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                code: process.env.SITE_CODE,
                secretKey: process.env.SECRET_KEY
            })
        })
        const { userRequestList, changeImageList, qrcodeList } = await result.json()
        if(!_.isEmpty(userRequestList)) {
            for(const user of userRequestList || []) {
                user['status'] = ''
                const temp = new TempModel( user )
                await temp.save()
                tick()
            }
        }
        if(!_.isEmpty(changeImageList)) {
            for(const user of changeImageList) {
                saveUserImage(user.phoneNumber, user.image)        
            }
        }
        if(!_.isEmpty(qrcodeList)) {
            const used = {}
            for(const gate of gateList || []) {
                used[gate.id.toString()] = false
            }
            for(const qrcode of qrcodeList || []) {
                qrcode['name'] = qrcode['visitorName']
                qrcode['plate'] = qrcode['visitorPlate']
                qrcode['used'] = used
                qrcode['expireDate'] = Math.floor(new Date() / 1000) + (qrcode['duration'] * 60)
                const visitor = new VisitorModel( qrcode )
                await visitor.save()
            }
        }
    } catch (e) {
        logger('ERR', e.message)
    }

}

module.exports = { checkUpdate }