'use strict'
const { readFileSync, writeFileSync, appendFileSync } = require('fs')

const readUserImage = async (phoneNumber) => {
    const path = `${process.env.NODE_DIR}/static/users/${phoneNumber}.png`
    return await readFileSync(path, { encoding: 'base64' })
}

const readTempFile = async () => {
    const path =`${process.env.NODE_DIR}/static/temp.json`
    return JSON.parse(await readFileSync(path))
}

const saveUserImage = async (phoneNumber, image) => {
    const path = `${process.env.NODE_DIR}/static/users/${phoneNumber}.png`
    await writeFileSync(path, image, { encoding: 'base64' })
}

const saveTempFile = async (userList) => {
    const path =`${process.env.NODE_DIR}/static/temp.json`
    await writeFileSync(path, JSON.stringify({
        timeStamp: Math.floor(new Date() / 1000),
        userList
    }))
}

const entryLog = async (id, gate, entry) => {
    const path = `${process.env.NODE_DIR}/static/log`
    await appendFileSync(path, `${Math.floor(new Date() / 1000)},${id},${gate},${entry}`)
}

module.exports = { readUserImage, saveUserImage, readTempFile, saveTempFile, entryLog }