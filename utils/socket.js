'use strict'

const UserModel = require('../api/models/userModel')
const TempModel = require('../api/models/tempModel')
const { saveTempFile, entryLog } = require('./file')

const RECEIVER_LIST = require('../config/gateList.json')

const USER_IMAGE_PATH = '/users/'
const MAX_TEMP = 30

const socket = (function() {
    let io
    let entryList = []
    let userList = []
    let tempList = []
    const receiverList = RECEIVER_LIST.map(item => ({
        ...item,
        status: 'offline',
        count: 0,
        lastUpdate: 0,
    }))

    const _getUserList = async () => {
        userList = await UserModel.find()
    }

    const _getTempList = async () => {
        tempList = await TempModel.find()
    }

    const _sendData = (pipe, message) => {
        io.emit(pipe, message)
    }

    const _getTime = () => {
        return new Date().toLocaleString('th', {hour: '2-digit', minute: '2-digit'}).toString()
    }

    const initial = async (app) => {
        await _getUserList()
        await _getTempList()
        io = require('socket.io')(app)
        io.on('connect', function(socket) {
            socket.emit('receiver', receiverList)
            socket.emit('update', { entryList, receiverList })
            socket.emit('users', userList)
            socket.emit('request', tempList)
        })
    }

    const sendReceiver = () => {
        const check_time = Math.floor(new Date() / 1000)
        for(const receiver of receiverList) {
            if(check_time - receiver.lastUpdate > 30) {
                receiver.status = 'offline'
            } else {
                receiver.status = 'online'
            }
        }
        _sendData('receiver', receiverList)
    }

    const updateReceiver = (receiverId) => {
        const receiver = receiverList.find(gate => gate.id === receiverId)
        receiver.lastUpdate = Math.floor(new Date() / 1000)
    }

    const tick = async () => {
        await _getUserList()
        await _getTempList()
        _sendData('users', userList)
        _sendData('request', tempList)
    }

    const tickTemp = async () => {
        await _getUserList()
        saveTempFile(userList)
    }

    const userEntry = (id, name, phoneNumber, gateId) => {
        const receiver = receiverList.find(gate => gate.id === gateId)
        receiver['count'] = receiver['count'] ?receiver['count'] + 1 : 1
        const entry = { 
            name,
            imagePath: `${USER_IMAGE_PATH}${phoneNumber}.png`,
            gateName: receiver.name,
            gateCategory: receiver.category,
            entry: receiver.entry,
            time: _getTime()
        }
        entryLog(id, gateId, receiver.entry)
        entryList.unshift(entry)
        entryList = entryList.splice(0, MAX_TEMP)
        _sendData('update', {entryList: [entry], receiverList})
    }

    return {
        initial,
        userEntry,
        updateReceiver,
        sendReceiver,
        tick,
        tickTemp
    }
})()

module.exports = socket